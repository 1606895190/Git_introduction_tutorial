// Chat Box
var counter = 0;
if (localStorage.simpanChat){
    document.getElementById('msg-insert').innerHTML = localStorage.simpanChat;
}
else {
    localStorage.simpanChat = "";
}
function submitOnEnter(event){
	var chatField = document.getElementById('msg-insert');
	var chatText = document.getElementById('chat-text');

	if (event.keyCode == 13 && chatText.value != "") {
    if (counter % 2 == 0){
      var cls = 'msg-send';
      var sender = 'Davin: ';
      chatText.value = sender + chatText.value;
      localStorage.simpanChat = (localStorage.simpanChat + '<p class="' + cls + '">' +chatText.value + "</p>")
      chatField.innerHTML=localStorage.simpanChat;
      chatText.value='';  
      counter += 1;
    }
    else {
      var cls = 'msg-receive';
      var sender = 'Gibran: ';
      chatText.value = sender + chatText.value;
      localStorage.simpanChat = (localStorage.simpanChat + '<p class="' + cls + '">' +chatText.value + "</p>")
      chatField.innerHTML=localStorage.simpanChat;
      chatText.value='';  
      counter += 1; 
    }
    
	}
}

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value='';
  } 
  else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } 
  else if (x === ('sin')){
    print.value = Math.round(Math.sin(evil(print.value) * Math.PI / 180) * 10000) / 10000;
  }
  else if (x === ('tan')){
    if (Math.round(Math.tan(evil(print.value) * Math.PI / 180) * 10000) / 10000 > 10){
      print.value = "Infinity";
    }
    else{
      print.value = Math.round(Math.tan(evil(print.value) * Math.PI / 180) * 10000) / 10000;
    }
  }
  else if (x === ('log')){
    print.value = Math.round(Math.log10(evil(print.value)) * 10000) / 10000;
  }
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

// Change Theme

if (typeof(Storage) !== "undefined") {
  var themes = [
                {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
                {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
                {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
                {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
                {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
                {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
                {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
                {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
                {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
                {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
                {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
                ];
    localStorage.themes = JSON.stringify(themes);
  

} 
else {
    window.alert("Sorry, your browser does not support web storage...");
}

$(document).ready(function() {
  if (localStorage.selectedTheme){
    var themesSaatNow = JSON.parse(localStorage.selectedTheme);
    $('body').css({"background-color" : themesSaatNow.Indigo.bcgColor, "color" : themesSaatNow.Indigo.fontColor});
  }
  else {
    var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
    localStorage.selectedTheme = JSON.stringify(selectedTheme);
    $('body').css({"background-color" : selectedTheme.Indigo.bcgColor, "color" : selectedTheme.Indigo.fontColor});
  }
  
    
    $('.my-select').select2({
    'data': JSON.parse(localStorage.themes),
})


    $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
      var my_select_value = $('.my-select').val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
      var selected_theme = themes[my_select_value];
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      $('body').css({"background-color" : selected_theme.bcgColor, "color" : selected_theme.fontColor});
    // [TODO] simpan object theme tadi ke local storage selectedTheme
      var textColor = selected_theme.text;
      var bcgColor = selected_theme.bcgColor;
      var fontColor = selected_theme.fontColor;
      var dummy = {"Indigo" : {"bcgColor" : bcgColor, "fontColor" : fontColor}};
      localStorage.selectedTheme = JSON.stringify(dummy);
})
});


// END
