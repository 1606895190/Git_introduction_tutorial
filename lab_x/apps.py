from django.apps import AppConfig


class LabXConfig(AppConfig):
    name = 'lab_x'
